using System;
using UnityEngine;
using UnityEngine.Events;

public abstract class Entity : MonoBehaviour
{
    public UnityEvent OnDeath;
    public UnityEvent<float> OnHealthChanged;
    public UnityEvent<float> OnEnergyChanged;

    [SerializeField] private float maxHealth;
    [SerializeField] private float currentHealth;

    [SerializeField] private float maxEnergy;
    [SerializeField] private float currentEnergy;

    public void GetDamage(float damage)
    {
        if (damage <= 0)
            throw new ArgumentException("Damage cant be less then 0!");

        currentHealth -= damage;
        OnHealthChanged?.Invoke(currentHealth);

        if (currentHealth < 0)
            OnDeath?.Invoke();
    }

    public void Hill(float health)
    {
        var needHealth = maxHealth - currentHealth;
        if (needHealth > 0 && needHealth <= health) currentHealth += health;
    }

    public void RecoveryEnergy(float energy)
    {
        var needEnergy = maxEnergy - currentEnergy;
        if (needEnergy > 0 && needEnergy <= energy) currentEnergy += energy;
    }

    public void SpendEnergy(float energyAmount)
    {
        if (energyAmount <= 0)
            throw new ArgumentException("Energy amount less than 0!");
        currentEnergy -= energyAmount;
        OnEnergyChanged?.Invoke(currentEnergy);
    }

    public bool IsEnoughtEnergy(float energyAmount)
    {
        if (energyAmount <= 0)
            throw new ArgumentException("Energy amount less than 0!");
        return currentEnergy >= energyAmount;
    }

}
