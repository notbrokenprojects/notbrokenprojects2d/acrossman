﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Classes
{
    [RequireComponent(typeof(Slider))]
    public abstract class IndicatorBar : MonoBehaviour
    {
        private Slider slider;

        private void Start()
        {
            slider = GetComponent<Slider>();
        }

        public void SetMax(float max)
        {
            slider.maxValue = max;
        }

        public void Change(float value)
        {
            slider.value = value;
        }
    }
}
