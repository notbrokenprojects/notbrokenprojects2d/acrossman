using UnityEngine;

/// <summary>
/// ����� ��� ���������� �����
/// </summary>
public class Background : MonoBehaviour
{
    /// <summary>
    /// �����
    /// </summary>
    float length;

    ///<summary>
    ///��������� �������
    /// </summary>
    float startpos;

    /// <summary>
    /// �������� ������
    /// </summary>
    public GameObject Cam;

    /// <summary>
    /// ������� ����������
    /// </summary>
    public float ParallaxEffect;

    // Start is called before the first frame update
    void Start()
    {
        startpos = transform.position.x;
        length = GetComponent<SpriteRenderer>().bounds.size.x;
    }

    public void FixedUpdate()
    {
        float temp = Cam.transform.position.x * (1 - ParallaxEffect);
        float dist = Cam.transform.position.x * ParallaxEffect;
        transform.position = new Vector3(startpos + dist, transform.position.y, transform.position.z);

        if (temp > startpos + length) startpos += length;
        else if (temp < startpos - length) startpos -= length;
    }
}
