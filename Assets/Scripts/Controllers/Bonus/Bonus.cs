using UnityEngine;

public class Bonus : MonoBehaviour
{
    private int value;

    private void Start()
    {
        value = Random.Range(10, 20);
    }

    public int GetBonus()
    {
        return value;
    }
}
