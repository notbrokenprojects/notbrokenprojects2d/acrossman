﻿using UnityEngine;

public class GameCamera : MonoBehaviour
{
    /// <summary>
    /// Скорость камеры
    /// </summary>
    readonly float speed = 3f;

    /// <summary>
    /// Цель
    /// </summary>
    [SerializeField] private Transform Target;

    // Start is called before the first frame update
    void Start()
    {
        Vector3 position = Target.transform.position;
        transform.position = new Vector3(position.x, position.y, transform.position.z);
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 postion = Target.position;
        postion.z = transform.position.z;

        transform.position = Vector3.Lerp(transform.position, postion, speed * Time.deltaTime);
    }
}
