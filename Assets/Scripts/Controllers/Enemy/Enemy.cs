using UnityEngine;

public class Enemy : Entity
{
    [SerializeField] private int meleeDamage;
    [SerializeField] private float forceImpulce;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Component component;
        if (collision.gameObject.TryGetComponent(typeof(Player), out component))
        {
            DamagePlayer(((Player)component));
        }
    }

    private void DamagePlayer(Player player)
    {
        player.GetDamage(meleeDamage * Time.fixedDeltaTime);
    }
}
