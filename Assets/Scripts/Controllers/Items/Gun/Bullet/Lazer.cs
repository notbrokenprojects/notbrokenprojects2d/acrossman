using System;
using UnityEngine;

public class Lazer : MonoBehaviour
{
    [SerializeField] private float Speed;

    /// <summary>
    /// ���� ����� �������� ���� �������
    /// </summary>
    [SerializeField] private string[] DamageName;

    /// <summary>
    /// ����� ���� ��������� � ������������ �������
    /// </summary>
    [SerializeField] private int[] DamageValue;

    void Start()
    {
        GetComponent<Rigidbody2D>().velocity = Vector3.right * Speed;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        string colTag = collision.gameObject.tag;

        //���� ����� ���� ����� ������� ����, ����������� �������� �����
        var index = Array.IndexOf(DamageName, colTag);
        if (index > -1)
        {
            var damage = DamageValue[index];
            var enemy = collision.gameObject.GetComponent<Enemy>();
        }
         else if (colTag != "Lazer") //����� ���� �� �����, �� ���������� �����
        {
            Destroy(gameObject);
        }
    }
}
