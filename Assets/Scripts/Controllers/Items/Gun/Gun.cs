using System.Collections;
using UnityEngine;

public class Gun : MonoBehaviour
{
    [SerializeField] private Transform GunPosition;
    [SerializeField] private GameObject Bullet;
    [SerializeField] private int LazerEnergy;
    private Player player;
    private bool isShooting;
    private const int waitShoot = 1;

    private void Start()
    {
        player = transform.parent.GetComponent<Player>();
        isShooting = true;
    }

    public void Shoot()
    {
        if (player.IsEnoughtEnergy(LazerEnergy) && isShooting)
        {
            Instantiate(Bullet, GunPosition);
            player.SpendEnergy(LazerEnergy);
            isShooting = false;
            StartCoroutine(WaitShoot());
        }
    }

    IEnumerator WaitShoot()
    {
        yield return new WaitForSeconds(waitShoot * Time.deltaTime);
        isShooting = true;
    }
}

