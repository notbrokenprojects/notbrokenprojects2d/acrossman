﻿using Assets.Scripts.Enums;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

public class Player : Entity
{
    [SerializeField] private float speed;
    [SerializeField] private float jumpPower;
    [SerializeField] private EnergyBar energyBar;
    [SerializeField] private HealthBar healthBar;
    [SerializeField] private Gun Gun;

    private PlayerInput input;

    private new Rigidbody2D rigidbody2D;
    private Animator animator;

    private bool isGrounded;
    private bool useOneSpeed;

    private void Awake()
    {
        input = new PlayerInput();
    }

    private void OnEnable()
    {
        input.Enable();
    }

    private void OnDisable()
    {
        input.Disable();
    }

    void Start()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();

        animator.SetInteger(nameof(PlayerState), (int)PlayerState.Run);
        input.Player.Jump.performed += Jump_performed;
        input.Player.Shoot.performed += Shoot_performed;
    }

    private void Shoot_performed(InputAction.CallbackContext obj)
    {
        Gun.Shoot();
    }

    private void Jump_performed(InputAction.CallbackContext context)
    {
        Debug.Log("Jump");

        if (isGrounded)
        {
            rigidbody2D.AddForce(Vector2.up * jumpPower, ForceMode2D.Impulse);
        }
    }

    private void Update()
    {
        Move(Vector2.right);
    }

    private void Move(Vector2 direction)
    {
        float scaleMoveSpeed = speed * Time.deltaTime;
        Vector3 moveDirection = new Vector3(direction.x, direction.y, 0);
        transform.position += moveDirection * scaleMoveSpeed;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        CheckBonusFill(collision, "Health", Hill);
        CheckBonusFill(collision, "Energy", RecoveryEnergy);
        CheckBonusSpeed(collision, "Slow", Slow());
        CheckBonusSpeed(collision, "Fast", Fast());
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        isGrounded = !collision.gameObject.CompareTag("Ground");
    }

    private void OnCollisionStay(Collision collision)
    {
        isGrounded = collision.gameObject.CompareTag("Ground");
    }

    private void CheckBonusSpeed(Collider2D collider2D, string bonusName, IEnumerator bonusEnumarotr)
    {
        if (collider2D.gameObject.CompareTag(bonusName))
        {
            Destroy(collider2D.gameObject);
            StartCoroutine(bonusEnumarotr);
        }
    }

    private void CheckBonusFill(Collider2D collider, string bonusName, Action<float> action)
    {
        if (collider.gameObject.CompareTag(bonusName))
        {
            var bonus = collider.gameObject.GetComponent<Bonus>().GetBonus();
            action(bonus);
        }
    }

    private IEnumerator Fast()
    {
        if (useOneSpeed) yield break;

        useOneSpeed = true;
        var speedOld = speed;
        speed *= 2;
        yield return new WaitForSeconds(10);
        speed = speedOld;
        useOneSpeed = false;
    }

    private IEnumerator Slow()
    {
        if (useOneSpeed) yield break;

        useOneSpeed = true;
        var speedOld = speed;
        speed /= 2;
        yield return new WaitForSeconds(10);
        speed = speedOld;
        useOneSpeed = false;
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        CheckGround(collision);
    }

    private void CheckGround(Collision2D collision)
    {
        isGrounded = collision.gameObject.CompareTag("Ground");
    }
}
