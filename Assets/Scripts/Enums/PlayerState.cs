﻿namespace Assets.Scripts.Enums
{
    /// <summary>
    /// Состояние игрока
    /// </summary>
    public enum PlayerState : int
    {
        /// <summary>
        /// Покой
        /// </summary>
        Idle = 1,

        /// <summary>
        /// Бег
        /// </summary>
        Run = 2,

        /// <summary>
        /// Умер
        /// </summary>
        Dead = 3
    }
}
